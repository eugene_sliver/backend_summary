<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function(){
   Route::post('/signup','apiController\ApiauthCtrl@register');
	Route::post('/signin','apiController\ApiLoginctrl@signin');

   Route::middleware('auth.jwt')->group(function(){
  Route::get('/events','apiController\ApieventCtrl@gotAll');
	Route::get('/details/{evid}','apiController\ApieventCtrl@showDetails')->name('details');
	Route::post('/cardreg','apiController\CardegCtrl@C_reg');
	Route::post('/refund/{vtid}/pay','apiController\Reftop@refund')
	            ->name('refund');
	Route::post('/topup/{vtid}/increase','apiController\Reftop@topup')
           ->name('topup');

   });
	
});
