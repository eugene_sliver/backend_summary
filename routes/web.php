<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::middleware('client_auth')->get('/dash', function () {
//     return view('welcome');
// });


//Auth system
Route::get('/','customAuth\LoginCtrl@showLoginForm')->middleware('client_guest');
Route::post('/client','customAuth\LoginCtrl@login')->middleware('client_guest');


Route::middleware('client_auth')->group(function(){

	Route::get('/dash', function () {
    return view('welcome');
                            });

Route::post('/logout','customAuth\LoginCtrl@logout')->name('logout');


Route::resource('webusers','webController\WebuserCtrl');

Route::get('/regfm','customCtrl@formreg');

Route::post('/postFom','customCtrl@postFom')->name('regus');
Route::get('/usersevents','customCtrl@userEvent');
Route::get('/specific/{uid}','customCtrl@getspecific');


Route::get('/getevents','EventsCtrl@getAllEvents');
Route::get('/test/{userid}','EventsCtrl@testfuc');
Route::post('/postevent','EventsCtrl@postEvents');
Route::get('/specevent/{id}','EventsCtrl@edit');
Route::put('/updtev/{evid}','EventsCtrl@updateev');
Route::delete('/delEvent/{id}','EventsCtrl@deleteEvent');
Route::get('/refeve','EventsCtrl@getAllEvents');

Route::get('/appuser','WebController\AppuserCtrl@getAllapuser');
Route::get('/colect','WebController\AppuserCtrl@collections');
Route::put('/appuserchange/{uidp}','WebController\AppuserCtrl@gotUpdated');
Route::get('/oneuser/{id}','WebController\AppuserCtrl@singleUser');
Route::delete('/delapuser/{uid}','WebController\AppuserCtrl@onDelete');

});


