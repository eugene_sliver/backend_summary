<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\User',10)->create();
        factory('App\Event',10)->create();
        factory('App\Appuser',10)->create();
       factory('App\Role',1)->create();
        factory('App\Payment',10)->create();
        factory('App\Profile',10)->create();
        factory('App\Refund',10)->create();
        factory('App\Balance',10)->create();

         DB::table('roles')->insert([
            'role_name' => 'client',     
        ]);

          DB::table('role_user')->insert([
            'role_id' => 1,  
            'user_id'=>11,  
        ]);
            //admin
          DB::table('users')->insert([
            'id' => 11,  
            'name'=>'kigali',
            'email'=>'kigali@gmail.com',
            'password'=>bcrypt('secret') 
        ]);
    }
}
