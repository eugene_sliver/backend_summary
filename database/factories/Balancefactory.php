<?php

use Faker\Generator as Faker;

$factory->define(App\Balance::class, function (Faker $faker) {
    return [
        'card_id'=>App\Payment::all()->random()->id,
        'balance'=>$faker->numberBetween($min = 30, $max = 70),
    ];
});
