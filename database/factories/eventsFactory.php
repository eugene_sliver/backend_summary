<?php

use Faker\Generator as Faker;

$factory->define(App\Event::class, function (Faker $faker) {
    return [
        'title'=>$faker->country,
        'location'=>$faker->city,
        'date'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'price'=>$faker->randomDigit,
        'time_clock'=>$faker->time($format = 'H:i:s', $max = 'now'),
        'user_id'=>App\User::all()->random()->id,
         'image'=>$faker->imageUrl($width=100, $height=100, 'cats',true),
         'description'=>$faker->sentence($nbWords = 6, $variableNbWords = true),
    ];
});
