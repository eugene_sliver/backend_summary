<?php

use Faker\Generator as Faker;

$factory->define(App\Appuser::class, function (Faker $faker) {
	static $password;
    return [
        'first_name'=>$faker->firstNameMale,
        'last_name'=>$faker->lastName,
        'iban_number'=>$faker->iban(null),
        'bic_number'=>$faker->swiftBicNumber,
        'email'=>$faker->unique()->safeEmail,
        'password'=>$password ?: $password = bcrypt('secret'),
    ];
});
