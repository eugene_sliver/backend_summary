<?php

use Faker\Generator as Faker;

$factory->define(App\Profile::class, function (Faker $faker) {
    return [
       'imageProfile'=>$faker->imageUrl($width=50, $height=50, 'cats',true),
       'user_id'=>App\User::all()->random()->id,
    ];
});
