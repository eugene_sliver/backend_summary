<?php

use Faker\Generator as Faker;

$factory->define(App\Payment::class, function (Faker $faker) {
    return [
       'card_number'=>$faker->numberBetween($min = 0, $max = 11),
       'appuser_id'=>$faker->randomDigit,
       
    ];
});
