<?php

use Faker\Generator as Faker;

$factory->define(App\Refund::class, function (Faker $faker) {
    return [
        'appuser_id'=>$faker->numberBetween($min = 0, $max = 11),
        'event_id'=>App\Event::all()->random()->id,
    ];
});
