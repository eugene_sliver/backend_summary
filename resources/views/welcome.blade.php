<!DOCTYPE html>
<html>
<head>
	<title>StartNet</title>
 <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <meta name="_token" content="{{ csrf_token() }}"/>
   <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>
<body>
<div id="app">
  <div>
	<nav class="navbar is-fixed-top" role="navigation" aria-label="dropdown navigation">
	<a class="navbar-item">
    <img src="https://www.mdhub.co.uk/uploads/pics/logo_placeholder.png" width="60" height="60">
  </a>
  <div class="navbar-menu">
    
    <div class="navbar-end">
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          Username
        </a>
       
        <div class="navbar-dropdown " position="is-bottom-left">

        	<router-link to="/endUser" class="navbar-item">Profile</router-link>
  
          <hr class="navbar-divider">
			      <a href="{{ route('logout') }}"
			onclick="event.preventDefault();
			         document.getElementById('logout-form').submit();">
			         <i class="navbar-item"> Logout</i>
			       </a>

			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			{{ csrf_field() }}
			</form>
        </div>
      </div>
    </div>
  </div>
</nav>

<div class="my-side-nav">
	
<aside class="menu">
  <ul class="menu-list">

   <li>
  
    <router-link to="/" class="navbar-item">
      <span class="panel-con">
      <i class="fa fa-calendar" aria-hidden="true"></i>
      </span>
    User's Events
   </router-link>
 </li>

    <li>
      
      <a href="#" class="cont">
        <span class="panel-con">
        <i class="fa fa-tasks" aria-hidden="true"></i>
      </span>

      Manage Users</a>
      <ul class="combiner is-white">
        <li><router-link to="/clients" class="navbar-item">Clients</router-link></li>
        <li><router-link to="/endUser" class="navbar-item">AppUsers</router-link></li>
        <li><router-link to="/allevent" class="navbar-item">AllEvents</router-link></li>
        <li><router-link to="/refunded" class="navbar-item">Refunded</router-link></li>
      </ul>
    </li>
    <li>
      
   
    <router-link to="/endUser" class="navbar-item">
      <span class="panel-con">
        <i class="fa fa-credit-card" aria-hidden="true"></i>
      </span>

    Payments</router-link>
  </li>
  </ul>
</aside>
	</div>

	<div class="contents">
		<router-view></router-view>
	</div>
	
</div>
</div>

<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
</body>
</html>