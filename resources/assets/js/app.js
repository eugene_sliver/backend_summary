import VueRouter from 'vue-router';
import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/lib/buefy.css';
import swal from 'sweetalert';
import example from './components/ExampleComponent.vue';
import dash from './components/DashboardComponent.vue';
import user_event from './components/UserseventComponent.vue';
import client_list from './components/clientListComponent.vue';
import enduser_list from './components/endUserComponent.vue';
import login from './components/loginComponent.vue';
import event_register from './components/addNeweventComponent.vue';
import appuser from './components/appUserComponent.vue';
import addevent from './components/addNeweventComponent.vue';
import eventform from './components/addeventComponent.vue';
import createuser from'./components/createUserComponent.vue';
import refund from'./components/refundComponent.vue';
import updateev from './components/update/EventupdateComponent.vue';
import remove from './components/update/deleteEventComponent.vue';
import upclient from './components/update/clientEdit.vue';
import alleve from './components/Allevents.vue';

require('./bootstrap');


const routes = [
  { path: '/', component: user_event },
  { path: '/userev', component: example},
  {path:'/clients',component:client_list},
  {path:'/enduser',component:enduser_list},
  {path:'/login',component:login},
  {path:'/store',component:event_register},
  {path:'/appuser/:uid',component:appuser},
  {path:'/addevents/:id',component:addevent},
  {path:'/newevent/:id',component:eventform},
  {path:'/createuser',component:createuser},
  {path:'/refunded',component:refund},
  {path:'/evup/:evid/edit',component:updateev},
  {path:'/delete/:id',component:remove},
  {path:'/editclient/:id',component:upclient},
  {path:'/allevent',component:alleve}
]

const router = new VueRouter({
  routes, // short for `routes: routes`
});


Vue.use(VueRouter);
Vue.use(Buefy);


const app = new Vue({
	 components:{example,dash,login},
    el: '#app',
    router,
    data:{
    	name:'kigali',
    	email:"eugene"
    }
   
});
