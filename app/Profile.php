<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function userProfile()
    {
    	return $this->belongsTo('App\User','foreign_key');
    }
}
