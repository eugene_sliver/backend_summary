<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
     protected $fillable = [
   'appuser_id', 'event_id',
    ];

    public function appusers()
    {
    	return $this->belongsTo('App\Appuser');
    }

     public function events()
    {
    	return $this->belongsTo('App\Event');
    }
}
