<?php

namespace App\starnet\events;
use App\starnet\RepostoryClass;
use Illuminate\Support\Facades\Validator;

/**
* 
*/
class Events extends RepostoryClass
{
	
	function passedModel(){

		return 'App\Event';
	}

	 public function create(array $data){
    	$clientEvent=$this->model;
        $id=2;
    		$clientEvent->title=$data['title'];
    		$clientEvent->location=$data['location'];
    		$clientEvent->date=$data['date'];
        $clientEvent->price=$data['price'];
    		$clientEvent->time_clock=$data['time_clock'];
    		$clientEvent->image=$data['image'];
    		$clientEvent->description=$data['description'];
    		$clientEvent->user_id=$data['user_id'];
            $clientEvent->save();
             return $clientEvent;

    }

      public function update(array $data,$id)
  {

  	$clientEvent=$this->model->findOrfail($id);
        $clientEvent->title=$data['title'];
        $clientEvent->location=$data['location'];
        $clientEvent->price=$data['price'];
        $clientEvent->date=$data['date'];
        $clientEvent->time_clock=$data['time_clock'];
        $clientEvent->image=$data['image'];
        $clientEvent->description=$data['description'];
        $clientEvent->save();

  	  return $clientEvent;
  }


    public function validation(array $data){

      return Validator::make($data,[
             'title'=>'required',
             'location'=>'required',
             'price'=>'required',
             'date'=>'required',
             'time_clock'=>'required',
             'description'=>'required',
      ]);
 
    }


     public function refunds()
       {
    $refund=$this->model->with('refund')->get()->toArray();
       
       return $refund;
   }


}