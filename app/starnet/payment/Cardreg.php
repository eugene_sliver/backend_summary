<?php

namespace App\starnet\payment;
use App\Events\Cardregisterd;
use App\starnet\RepostoryClass;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

/**
* 
*/
class Cardreg extends RepostoryClass
{
	function passedModel(){
		return 'App\Payment';
	}


	 public function create(array $data){

    	$card=$this->model;

    		$card->card_number=$data['card_number'];
    		$card->appuser_id=JWTAuth::parseToken()->toUser()->id;
             $card->save();
             event(new Cardregisterd($card));
             
             return $card;

    }

    public function validation(array $data)
    {
        return Validator::make($data,[
            'card_number'=>'required|unique:payments'
        ]);
    }
}