<?php

namespace App\starnet\refund;

use App\starnet\RepostoryClass;
use JWTAuth;

/**
* 
*/
class Refund extends RepostoryClass
{
	
	function passedModel(){

		return 'App\Refund';
	}


	 public function Refundactivity($id){

    	$refund=$this->model;
    		$refund->appuser_id=JWTAuth::parseToken()->toUser()->id;
    		$refund->event_id=$id;
             $refund->save();

             return $refund;

    }

    
	
}