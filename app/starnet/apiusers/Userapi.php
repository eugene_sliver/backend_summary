<?php

namespace App\starnet\apiusers;

use App\starnet\RepostoryClass;
use Illuminate\Support\Facades\Validator;

/**
* 
*/
class Userapi extends RepostoryClass
{
	
	function passedModel(){
		return 'App\Appuser';
	}

	public function SignUp(array $data){
    	$apiuser=$this->model;

    		$apiuser->first_name=$data['first_name'];
    		$apiuser->last_name=$data['last_name'];
    		$apiuser->iban_number=$data['iban_number'];
    		$apiuser->bic_number=$data['bic_number'];
    		$apiuser->email=$data['email'];
    		$apiuser->password=bcrypt($data['password']);
            $apiuser->save();
             return $apiuser;

    }

      public function update(array $data,$id)
  {

  	$apiuser=$this->model->findOrfail($id);
  	       $apiuser->first_name=$data['first_name'];
    		$apiuser->last_name=$data['last_name'];
    		$apiuser->iban_number=$data['iban_number'];
    		$apiuser->bic_number=$data['bic_number'];
    		$apiuser->email=$data['email'];
        $apiuser->save();
  	
  	return $apiuser;
  }

  public function payment()
   {
    $userEv=$this->model->with(array('card','refund'))->get()->toArray();
       
       return $userEv;
   }



    public function validation(array $data){

      return Validator::make($data,[
             'first_name'=>'required',
             'last_name'=>'required',
             'iban_number'=>'required',
             'bic_number'=>'required',
             'email'=>'required|unique:appusers',
      ]);
 
    }

}