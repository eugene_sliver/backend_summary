<?php

namespace App\starnet\Webusers;

use App\starnet\RepostoryClass;
use Illuminate\Support\Facades\Validator;

/**
* 
*/
class Webusers extends RepostoryClass 
{
	

	function passedModel(){

		return 'App\User';
	}

	public function all(){
    	return $this->model->all();
    }

    public function create(array $data){

    	$firstuser=$this->model;

    		$firstuser->name=$data['name'];
    		$firstuser->email=$data['email'];
    		$firstuser->password=bcrypt($data['password']);
             $firstuser->save();
             $firstuser->roles()->attach(2);

             return $firstuser;

    }

    public function validation(array $data){

      return Validator::make($data,[
             'name'=>'required',
             'email'=>'required|email|email|max:255|unique:users',
             'password'=>'required|min:6',
      ]);
 
    }

    public function update(array $data,$id)
  {

  	$singleUser=$this->model->findOrfail($id);
  	$singleUser->name=$data['name'];
  	$singleUser->email=$data['email'];
   	$singleUser->save();
  	
  	return $singleUser;
  }


   public function kill($id)
  {
  	$Webuser=$this->model->findOrfail($id);
  	$Webuser->roles()->detach(2);
  	$Webuser->delete();
  }

  public function events()
   {
    $userEv=$this->model->with('events')->get()->toArray();
       
       return $userEv;
   }

   public function specific($id)
   {

    $Ev=$this->model->find($id)->events;
     return $Ev;
   }
}