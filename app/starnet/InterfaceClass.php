<?php

namespace App\starnet;

interface InterfaceClass
{

	public function all();
	public function create(array $data);
	public function show($id);
	public function edit($id);
	public function update(array $data,$id);
	public function kill($id);
	public function validation(array $data);
	public function Refundactivity($id);
	
}