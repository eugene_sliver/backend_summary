<?php

namespace App\starnet\Facades;
use Illuminate\Support\Facades\Facade;

/**
* 
*/
class WebuserFacades extends Facade
{
	
	protected static function getfacadeAccessor(){

		return 'webusers';

	}
}