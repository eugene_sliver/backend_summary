<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Event extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
             'title'=>$this->title,
             'image'=>$this->image,
             'description'=>$this->description,
             'location'=>$this->location,
             'price'=>$this->price,
             'link'=>[
              'show'=>route('details',$this->id)
             ],
             'orgnizer'=>optional($this->user)->name
        ];
    }
}
