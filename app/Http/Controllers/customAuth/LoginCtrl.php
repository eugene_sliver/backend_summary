<?php

namespace App\Http\Controllers\customAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginCtrl extends Controller
{
    
    use AuthenticatesUsers;


protected $redirectTo = '/dash?';

      protected function guard()
    {
      return Auth::guard('client');
    }

     public function showLoginForm()
   {
       return view('auth.login');
   }




}
