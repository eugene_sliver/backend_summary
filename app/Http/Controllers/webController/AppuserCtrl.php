<?php

namespace App\Http\Controllers\WebController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\starnet\apiusers\Userapi;

class AppuserCtrl extends Controller
{
       private $appuses;

       public function __construct(Userapi $uip)
        {
        	$this->appuses=$uip;
        } 

        public function SinuP()
        {
        	
        }
      
      public function SignIn()
      {
      	
      }

   // ==================
   //    getAllapuser
   //    ===============


	public function getAllapuser()
	{
		return $this->appuses->all();
	}

// =======================
// 	collections
// 	================

	public function collections()
	{
		return $this->appuses->payment();
	}

// =========================
// update
// ========================

	public function gotUpdated(Request $req,$id)
	{
		if ($this->appuses->validation($req->all()))
		 {
			$this->appuses->update($req->all(),$id);
		} else{
			return response()->json(['error'=>'notvalid']);
		}
	}

// ======================
// SHOW
// =======================
	public function singleUser($id)
	{
		return $this->appuses->show($id);
	}

// ==============
// dELETE
// ==============
	public function onDelete($id)
	{
		return $this->appuses->kill($id);
	}
    
}
