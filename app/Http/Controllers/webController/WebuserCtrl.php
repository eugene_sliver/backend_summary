<?php

namespace App\Http\Controllers\webController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\starnet\Webusers\Webusers;
use App\User;

class WebuserCtrl extends Controller
{
    
    private $wus;
public function __construct(Webusers $webs)
{
    $this->wus=$webs;
}

    public function index()
    {
     return $this->wus->all(); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
          
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $valid= $this->wus->validation($request->all());

        if ($valid->fails()) {
            
            return "invald data";
        }

     return $this->wus->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        return $this->wus->show($id);
    }

    public function update(Request $request, $id)
    {
        \Log::info($request->all());
          return $this->wus->update($request->only('name','email'),$id);
    }

    public function destroy($id)
    {
        return $this->wus->kill($id);
    }
}
