<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\starnet\events\Events;

class EventsCtrl extends Controller
{
    private $events;
    public function __construct(Events $event)
    {
    	$this->events=$event;
    }



   // ========================================
   //                 GETALL
   // =================================

    public function getAllEvents()
    {
    	
      return $this->events->refunds();
    }


   // ========================================
   //                 CREATE NEW
   // =================================

    public function postEvents(Request $request)
    {

    	\Log::info($request->all());

    	$explode=explode(',',$request->image);
    	$decode=base64_decode($explode[1]);

    	if (str_contains($explode[0],'jpeg')) {
    		$extetion='jpg';

    	}else
    		$extetion="png";

    		$filename=str_random().'.'.$extetion;

    		$path=public_path().'/'.$filename;
    		file_put_contents($path,$decode);
 if ($this->events->validation($request->except('image')+['image'=>$filename]))    {
          $this->events->create($request->except('image')+['image'=>$filename]);
          } 	
    	
    }

   // ========================================
   //                 EDIT
   // =================================

    public function edit($id)
    {

    	return $this->events->show($id);

    }


   // ========================================
   //                UPDATE
   // =================================

   public function updateev(Request $request,$id)
    {
        \Log::info(strlen($request->image));
        // if (!$this->events->validation($request->except('image')) {
        //   return response()->json(['error'=>'notvalid']);
        // }

        if (strlen($request->image) < 100) {
        $this->events->update($request->all(),$id);
        } elseif (strlen($request->image) > 100) {
        $explode=explode(',',$request->image);
        $decode=base64_decode($explode[1]);

        if (str_contains($explode[0],'jpeg')) {
            $extetion='jpg';

        }else

            $extetion="png";

            $filename=str_random().'.'.$extetion;

            $path=public_path().'/'.$filename;
            file_put_contents($path,$decode);

            if ($filename==$request->image) {
                $this->events->update($request->all(),$id);
            }

 if ($this->events->validation($request->except('image')+['image'=>$filename])) {
    $this->events->update($request->except('image')+['image'=>$filename],$id);
          } else{
            return response()->json(['error'=>'notvalid']);
          }    
        }
    }


   
   // ========================================
   //                 DELETE
   // =================================
    public function deleteEvent($id)
    {

        \Log::info($this->events->kill($id));
        return $this->events->kill($id);
    }
}