<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\starnet\Webusers\Webusers;

class customCtrl extends Controller
{

	    private $wus;
public function __construct(Webusers $webs)
{
    $this->wus=$webs;
}

 public function index()
 {
 	
 	return $this->wus->all();
 }
   
   public function postFom(Request $request)
   {
   	if ($this->wus->validation($request->all())) {
   		 $this->wus->create($request->all());
   	}else{
         return response()->json(['error'=>'notvalid']);
      }
  
     
   }


   public function userEvent()
   {
   	$userEv=$this->wus->events();
       
   	return $userEv;
   }

   public function getspecific($id)
   {

   	return $this->wus->specific($id);
   }
}
