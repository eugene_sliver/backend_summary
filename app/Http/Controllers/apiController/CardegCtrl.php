<?php

namespace App\Http\Controllers\apiController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\starnet\payment\Cardreg;

class CardegCtrl extends Controller
{

private $card_info;
   public function __construct(Cardreg $card)
   {
   	$this->card_info=$card;
   }


   public function C_reg(Request $req)
   {
   	  $vld=$this->card_info->validation($req->except('token'));
   	  if ($vld->fails()) {
   	  	return response()->json(['error'=>'Invalid data']);
   	  }
       
       \Log::info($req->except('token'));

   	 return $this->card_info->create($req->except('token'));
   }
}
