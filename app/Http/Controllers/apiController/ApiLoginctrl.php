<?php

namespace App\Http\Controllers\apiController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\starnet\apiusers\Userapi;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;



class ApiLoginctrl extends Controller
{
  
  private $userapi;

  public function __construct(Userapi $user)
  {
  	$this->userapi=$user;
  }

      public function signin(Request $req)
    {

    	\Log::info($req->all());

    	$credetials=$req->only('email','password');

    	try{

    		if (! $token = JWTAuth::attempt($credetials)) {
    			return response()->json(['error'=>'Invali data bro!']);
    		}
    	}
    	catch (JWTException $e){
    		return response()->json(['error'=>'Something went wrong']);
    	}

    	 return response()->json(['token'=>$token]);


    }
}
