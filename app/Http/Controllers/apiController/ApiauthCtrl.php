<?php

namespace App\Http\Controllers\apiController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\starnet\apiusers\Userapi;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Foundation\Auth\RegistersUsers;
use JWTAuth;

class ApiauthCtrl extends Controller
{

use RegistersUsers;

  private $userapi;

  public function __construct(Userapi $user)
  {
  	$this->userapi=$user;
  }
    public function register(Request $req)
    {
    	$vald=$this->userapi->validation($req->all());

    	if ($vald->fails()) {
    		
    	return response()->json(['error'=>'Invalid data']);
    	}
        $credetial=$req->only('email','password');
    	$this->userapi->SignUp($req->all());
    	return response()->json(['message'=>'create succed','token'=>JWTAuth::attempt($credetial)]);
    }
}
