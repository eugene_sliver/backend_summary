<?php

namespace App\Http\Controllers\apiController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\starnet\events\Events;
use App\Http\Resources\EventCollection;
use App\Http\Resources\ShowResource as eventResource;


class ApieventCtrl extends Controller
{

    protected $events;
	public function __construct(Events $ev)
	{
		$this->events=$ev;
	}
   public function gotAll()
   {

   	$evts=$this->events->all();
   	return new EventCollection($evts);
   }

   public function showDetails($id)
   {
   	$evts=$this->events->show($id);
   	return new eventResource($evts);
   }
}
