<?php

namespace App\Http\Controllers\apiController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\starnet\refund\Refund;
use App\Payment;
use App\Event;
use JWTAuth;
use App\Events\UpdateBalance;
use App\Events\Topup;
use Illuminate\Support\Facades\Validator;

class Reftop extends Controller
{
    
 protected $seck;

 public function __construct(Refund $fd)
 {
 	$this->seck=$fd;
 }

    public function refund($id,Request $req)
    {
          
          //we will verfy the user later
 
           
          $price=Event::find($id)->price;
          $balance=Payment::where('appuser_id',JWTAuth::parseToken()->toUser()->id)->with('balance')->get();

     $cn=null;
     $balance_id=null;
          foreach ($balance as $key) {
          	$cn=$key->balance->balance;
          	$balance_id=$key->balance->id;

          	if ((int)$cn < (int)$price) {
          		
          		return 'Hey MAn you forget to topup couse your ancount is below';
          	}else if ((int)$cn >= (int)$price) {
          		
          		 $cn=(int)$cn - (int)$price;      		 
          	}

          }
    
          \Log::info([$balance_id]);

          $ui=JWTAuth::parseToken()->toUser()->id;

          event(new UpdateBalance($balance_id,$cn,$id,$ui));


    	return $cn."$";
    }

    public function topup(Request $req)
    {

    	$vald=Validator::make($req->all(),[
            'amount'=>'required',
    	]);

    	if ($vald->fails()) {
    		
    		return 'amount field is required like this amount:50+..';
    			}
    	
    	$balance=Payment::where('appuser_id',JWTAuth::parseToken()->toUser()->id)->with('balance')->get();

         $balance_id=null;
           foreach ($balance as $key) {
          	$balance_id=$key->balance->id;

          }

          $cn=$req->amount;

      event(new Topup($cn,$balance_id));

    	return $req->except('token');
    }

}
