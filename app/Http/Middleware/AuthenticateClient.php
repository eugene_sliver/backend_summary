<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthenticateClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

             foreach (Auth::guard('client')->user()->roles as $role) {
                 
                 if ($role->role_name=='admin') {
                     
                     return $next($request);
                 }

                 return redirect('/');
             }
        
    }
}
