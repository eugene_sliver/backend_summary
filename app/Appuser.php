<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Appuser extends Authenticatable
{
    use Notifiable;
    
    protected $fillable = [
    'first_name','last_name','iban_number','bic_number', 'email','password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function card()
    {
    	return $this->hasOne('App\Payment','appuser_id');
    }

 public function refund()
    {
        return $this->hasOne('App\Refund','appuser_id');
    }

}
