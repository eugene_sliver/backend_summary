<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UpdateBalance
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

 public $bal_id;
 public $newBal;
 public $event_id;
 public $_id_user;
    
    public function __construct($uid,$newBalance,$eve_id,$user_id)
    {
        $this->bal_id=$uid;
     $this->newBal=$newBalance;
     $this->event_id=$eve_id;
     $this->_id_user=$user_id;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
   
}
