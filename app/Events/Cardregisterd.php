<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Payment;

class Cardregisterd
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

   public $regcd;
    public function __construct(Payment $card)
    {
        $this->regcd=$card;
    }
}
