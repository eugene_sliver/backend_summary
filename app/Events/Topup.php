<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Topup
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

   public $amount;
   public $b_id;
    public function __construct($mony,$id)
    {
    $this->amount=$mony;
    $this->b_id=$id;
    }
}
