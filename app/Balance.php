<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $fillable = [
    'balance','card_id',
    ];

    public function card()
    {
    	return $this->belongsTo('App\Payment');
    }
}
