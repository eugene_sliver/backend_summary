<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

	protected $fillable = [
    'card_number','appuser_id',
    ];

    public function appuser()
    {
    	return $this->belongsTo('App\Appuser');
    }

    public function balance()
    {
    	return $this->hasOne('App\Balance','card_id');
    }
}
