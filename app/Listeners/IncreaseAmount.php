<?php

namespace App\Listeners;

use App\Events\Topup;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Balance;

class IncreaseAmount
{
    
    public function __construct()
    {
        //
    }

   
    public function handle(Topup $event)
    {
        
         $topup=Balance::find($event->b_id);
         $topup->balance=$event->amount;
         $topup->save();
    }
}
