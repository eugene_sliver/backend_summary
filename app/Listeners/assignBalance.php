<?php

namespace App\Listeners;

use App\Events\Cardregisterd;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Balance;

class assignBalance
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Cardregisterd  $event
     * @return void
     */
    public function handle(Cardregisterd $event)
    {
        $bal=new Balance;

        $bal->balance=0;
        $event->regcd->balance()->save($bal);
    }
}
