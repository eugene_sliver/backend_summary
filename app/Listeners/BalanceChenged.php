<?php

namespace App\Listeners;

use App\Events\UpdateBalance;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Balance;
use App\Payment;

class BalanceChenged
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateBalance  $event
     * @return void
     */
    public function handle(UpdateBalance $event)
    {
        $mybalance=Balance::find($event->bal_id);
        $mybalance->balance=$event->newBal;
        $mybalance->save();
        
    }
}
