<?php

namespace App\Listeners;

use App\Events\UpdateBalance;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Refund;

class RefundChenged
{
   
    public function __construct()
    {
        //
    }

    public function handle(UpdateBalance $event)
    {
        $refund=new Refund();
        $refund->appuser_id=$event->_id_user;
        $refund->event_id=$event->event_id;
        $refund->save();
    }
}
