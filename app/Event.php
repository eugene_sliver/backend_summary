<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

	  protected $fillable = [
    'title','location','date','time_clock',
     'image','description','user_id'
    ];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function refund()
	{
		return $this->hasMany('App\Refund','event_id');
	}


}
